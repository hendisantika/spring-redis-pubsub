package com.hendisantika.springredispubsub.redis;

import lombok.extern.slf4j.Slf4j;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-redis-pubsub
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 12/07/18
 * Time: 19.06
 * To change this template use File | Settings | File Templates.
 */
@Slf4j
public class MethodMessageListener {

    public void sendMessage(String message) {
        log.info(message);
    }
}