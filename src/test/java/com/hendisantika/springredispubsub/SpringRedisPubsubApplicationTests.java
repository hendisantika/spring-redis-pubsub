package com.hendisantika.springredispubsub;

import com.hendisantika.springredispubsub.redis.JsonRedisTemplate;
import com.hendisantika.springredispubsub.redis.SendMessage;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringRedisPubsubApplicationTests {

    @Test
    public void contextLoads() {
    }

    @Autowired
    private JsonRedisTemplate jsonRedisTemplate;

    @Test
    public void pubsub() {
        SendMessage sendMessage = new SendMessage();
        sendMessage.setName("naruto");
        sendMessage.setEmail("naruto@test.com");
        jsonRedisTemplate.convertAndSend("sendMessage", sendMessage);
    }

}
